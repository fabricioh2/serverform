"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var path_1 = __importDefault(require("path"));
var register_model_1 = require("../models/register.model");
var file_system_1 = __importDefault(require("../classes/file-system"));
var registroRouter = express_1.Router();
var fs = require('fs');
var bodyParser = require('body-parser');
var fileSystem = new file_system_1.default();
registroRouter.post('/create', function (req, resp) {
    var reg = {
        nombre: req.body.nombre,
        cedula: req.body.cedula,
        fechaNacimiento: req.body.fechaNacimiento,
        celular: req.body.celular,
        telefono: req.body.telefono,
        mail: req.body.mail,
        direccion: req.body.direccion,
        placa: req.body.placa,
        valido: req.body.valido,
        chasis: req.body.chasis,
        fechaRegistro: new Date(),
        fotoCedula: req.body.fotoCedula,
        fotoMatricula: req.body.fotoMatricula,
        propietario: req.body.propieatario
    };
    // Persistir
    register_model_1.Register.create(reg).then(function (result) {
        resp.json({
            ok: true,
            reg: reg
        });
    })
        .catch(function (err) {
        resp.json({
            ok: false,
            err: err
        });
    });
});
registroRouter.get('/existe', function (req, resp) {
    register_model_1.Register.findOne({ placa: req.query.placa }, function (err, userDb) {
        if (err)
            throw err;
        if (!userDb) {
            return resp.json({
                ok: false
            });
        }
        else {
            return resp.json({
                ok: true
            });
        }
    });
});
registroRouter.post('/img', function (req, resp) {
    var file = req.body.img;
    var name = req.body.name;
    var type = req.body.type;
    var pathPost = path_1.default.resolve(__dirname, '../uploads/', name);
    var existe = fs.existsSync(pathPost);
    if (!existe) {
        fs.mkdirSync(pathPost);
    }
    var binaryData = new Buffer(file, 'base64')
        .toString('binary');
    fs.writeFile(pathPost + "/" + name, binaryData, "binary", function (err) {
        console.log(err);
    });
    resp.json({ ok: 'true' });
});
/*
registroRouter.post('/img', async (req: any, res: Response) => {
    console.log('REQ', req.body);
    
    // if (! req.files ){
    //     return res.status(400).json({
    //         ok:false,
    //         mensaje: 'No se subio ningún archivo'
    //     });
    // }
    const file: FileUpload =  req.body.file;
    file.name = req.body.name;
    console.log('FILE', file);
    
    // if (! file ){
    //     return res.status(400).json({
    //         ok:false,
    //         mensaje: 'No se subio ningún archivo - image'
    //     });
    // }

 

    await fileSystem.guardarImagenTemporal( file, req.registroID);

    res.json({
        ok: true,
        file: file.mimetype
    });
});
*/
registroRouter.get('/pot', function (req, resp) {
    return resp.json({
        ok: true
    });
});
exports.default = registroRouter;
