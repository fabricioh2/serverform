"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var path_1 = __importDefault(require("path"));
var fs_1 = __importDefault(require("fs"));
var FileSystem = /** @class */ (function () {
    function FileSystem() {
    }
    FileSystem.prototype.guardarImagenTemporal = function (file, regID) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            //Crear carpeta
            var path = _this.crearCarpetaUsuario(regID);
            //Nombre del archivo
            var nombreArchivo = (file.name);
            file.mv(path + "/" + nombreArchivo, function (err) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve();
                }
            });
        });
    };
    FileSystem.prototype.crearCarpetaUsuario = function (regID) {
        var pathUser = path_1.default.resolve(__dirname, '../uploads/', regID);
        var existe = fs_1.default.existsSync(pathUser);
        if (!existe) {
            fs_1.default.mkdirSync(pathUser);
        }
        return pathUser;
    };
    return FileSystem;
}());
exports.default = FileSystem;
