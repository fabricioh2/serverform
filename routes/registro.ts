import { Router, Request, Response } from "express";
import path from 'path';
import { Register } from "../models/register.model";
import { FileUpload } from '../interfaces/file-upload';
import FileSystem from '../classes/file-system';



const registroRouter = Router();
const fs = require('fs');
const bodyParser = require('body-parser');
const fileSystem = new FileSystem(); 

registroRouter.post('/create', (req: Request, resp: Response) => {
    const reg = {
        nombre: req.body.nombre,
        cedula: req.body.cedula,
        fechaNacimiento: req.body.fechaNacimiento,
        celular:req.body.celular,
        telefono: req.body.telefono,
        mail: req.body.mail,
        direccion: req.body.direccion,
        placa: req.body.placa,
        valido: req.body.valido,
        chasis: req.body.chasis,
        fechaRegistro: new Date(), 
        fotoCedula: req.body.fotoCedula,
        fotoMatricula: req.body.fotoMatricula,
        propietario: req.body.propieatario
    };
    // Persistir
    Register.create(reg).then(result => {
        resp.json({
            ok: true,
            reg
        });
    })
    .catch(err => {
      
        resp.json({
            ok: false,
            err
        });
    });

});

registroRouter.get('/existe', (req: any, resp: Response) =>{

        Register.findOne({placa : req.query.placa} , (err, userDb) =>{
            if(err) throw err;

            if(!userDb){
                return resp.json(
                    {
                        ok: false
                    }
                );
            }else{
                return resp.json(
                    {
                        ok: true
                    }
                ); 
            }
        });

});


registroRouter.post('/img', (req: Request, resp: Response) => {
    const file = req.body.img;
    const name = req.body.name;
    const type = req.body.type;
    const pathPost = path.resolve(__dirname, '../uploads/',name);
    const existe = fs.existsSync(pathPost);
        if (!existe) {
            fs.mkdirSync(pathPost);
        }       
    let binaryData = new Buffer(file, 'base64')
    .toString('binary');

    fs.writeFile(`${pathPost}/${name}`, binaryData, "binary", (err: any) => {
        console.log(err);
    })

    resp.json({ok:'true'}); 
});

   



/*
registroRouter.post('/img', async (req: any, res: Response) => {
    console.log('REQ', req.body);
    
    // if (! req.files ){
    //     return res.status(400).json({
    //         ok:false,
    //         mensaje: 'No se subio ningún archivo'
    //     });
    // }
    const file: FileUpload =  req.body.file;
    file.name = req.body.name;
    console.log('FILE', file);
    
    // if (! file ){
    //     return res.status(400).json({
    //         ok:false,
    //         mensaje: 'No se subio ningún archivo - image'
    //     });
    // }

 

    await fileSystem.guardarImagenTemporal( file, req.registroID);

    res.json({
        ok: true,
        file: file.mimetype
    });
});
*/
registroRouter.get('/pot', (req: any, resp: Response) =>{

            return resp.json(
                {
                    ok: true
                }
            );
       
});


export default registroRouter;