import { FileUpload } from "../interfaces/file-upload";
import path from 'path';
import fs from 'fs';
import e from "express";

export default class FileSystem {
    constructor() { }

    guardarImagenTemporal(file: FileUpload, regID: string) {
        return new Promise((resolve, reject) => {
            //Crear carpeta
            const path = this.crearCarpetaUsuario(regID);
            //Nombre del archivo
            const nombreArchivo = (file.name);

            file.mv(`${path}/${nombreArchivo}`, (err: any) => {
                if (err) {
                    reject( err );
                } else {
                    resolve();
                }
            });
        });

    }

    private crearCarpetaUsuario(regID: string) {
        const pathUser = path.resolve(__dirname, '../uploads/', regID);
        const existe = fs.existsSync(pathUser);
        if (!existe) {
            fs.mkdirSync(pathUser);
        }

        return pathUser;
    }

   

    // moverImgTmpAPost (userId : string){
    //     const pathTmp = path.resolve(__dirname, '../uploads/',userId, 'temp');
    //     const pathPost = path.resolve(__dirname, '../uploads/',userId, 'posts');

    //     if(!fs.existsSync( pathTmp )){
    //         return [];
    //     }

    //     if(!fs.existsSync( pathPost )){
    //          fs.mkdirSync( pathPost);
    //     }

    //     const imagenesTemp= this.obtenerImagenesTmp( userId );

    //         //MOVER LAS IMAGENES
    //     imagenesTemp.forEach( img => {
    //         fs.renameSync ( `${pathTmp}/${ img}`, `${pathPost}/${ img}`)
    //     });
        
    //     return imagenesTemp;
    // }

    // private obtenerImagenesTmp( userId : string){
    //     const pathTmp = path.resolve(__dirname, '../uploads/',userId, 'temp');
    //     return fs.readdirSync (pathTmp) || [];
    // }

    // getFotoURL (userId: string, img: string){
    //     const url = path.resolve(__dirname, '../uploads/',userId, 'posts',img);

    //     if(!fs.existsSync( url )){
    //         return path.resolve(__dirname, '../assets/400x200.jpg');
    //    }
        
    //     return url;
    // }

}