"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var regSchema = new mongoose_1.Schema({
    nombre: {
        type: String,
        required: [true, 'Nombre Requerido']
    },
    cedula: {
        type: String,
        required: [true, 'Cedula Requerida']
    },
    fechaNacimiento: {
        type: Date
    },
    celular: {
        type: String,
        required: [true, 'Celular Requerida']
    },
    telefono: {
        type: String
    },
    mail: {
        type: String,
        required: [true, 'Mail Requerido']
    },
    direccion: {
        type: String,
        required: [true, 'Direccion Requerida']
    },
    placa: {
        type: String,
        required: [true, 'Placa Requerida'],
        unique: true
    },
    valido: {
        type: Number,
        required: [true, 'Estado valido requerido']
    },
    chasis: {
        type: String
    },
    fechaRegistro: {
        type: Date
    },
    fotoCedula: {
        type: String,
        required: [true, 'Foto Requerida']
    },
    fotoMatricula: {
        type: String,
        required: [true, 'Foto Requerida']
    },
    propietario: {
        type: String
    }
});
regSchema.plugin(require('mongoose-beautiful-unique-validation'));
exports.Register = mongoose_1.model('Register', regSchema);
