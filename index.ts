import { Server } from './classes/server';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import cors from 'cors';
import registroRouter from './routes/registro';


const server = new Server();

//Body parser
server.app.use(bodyParser.urlencoded({extended: true}));
server.app.use(bodyParser.json({limit: '50mb'}));
server.app.use(bodyParser({limit: '50mb',parameterLimit:1000000, extended: true}));

server.app.use(bodyParser.urlencoded({
    limit: '50mb',
    parameterLimit: 1000000,
    extended: true 
  }));

  //server.app.options('*', cors()) // include before other routes
 server.app.use(cors({ origin:true , credentials:true}));



//Conectar a Mongo
//, user: 'peaje', pass: 'epmmop.2020#'

mongoose.connect('mongodb://127.0.0.1:27017/peaje', 
    { useNewUrlParser: true, useCreateIndex: true},
    (err) => {
        if (err) 
        {
            console.log(err);            
            throw err;
        }else{
            console.log('Mongo PEAJE ONLINE');
        }
        
    }); 

    //Rutas de usuario
server.app.use( '/registro', registroRouter );

// Levantar Servidor
server.start(() => {
    console.log(`Servidor corriendo, puerto ${server.port}`);
});

