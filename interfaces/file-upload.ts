export interface FileUpload {
    name: string;
    data: any;
    size: number;
    encoding: string;
    tempFilePath: string;
    truncated: boolean;
    mimetype: string;

    mv: Function;
}