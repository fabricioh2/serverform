"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var server_1 = require("./classes/server");
var mongoose_1 = __importDefault(require("mongoose"));
var body_parser_1 = __importDefault(require("body-parser"));
var cors_1 = __importDefault(require("cors"));
var registro_1 = __importDefault(require("./routes/registro"));
var server = new server_1.Server();
//Body parser
server.app.use(body_parser_1.default.urlencoded({ extended: true }));
server.app.use(body_parser_1.default.json({ limit: '50mb' }));
server.app.use(body_parser_1.default({ limit: '50mb', parameterLimit: 1000000, extended: true }));
server.app.use(body_parser_1.default.urlencoded({
    limit: '50mb',
    parameterLimit: 1000000,
    extended: true
}));
//server.app.options('*', cors()) // include before other routes
server.app.use(cors_1.default({ origin: true, credentials: true }));
//Conectar a Mongo
//, user: 'peaje', pass: 'epmmop.2020#'
mongoose_1.default.connect('mongodb://127.0.0.1:27017/peaje', { useNewUrlParser: true, useCreateIndex: true }, function (err) {
    if (err) {
        console.log(err);
        throw err;
    }
    else {
        console.log('Mongo PEAJE ONLINE');
    }
});
//Rutas de usuario
server.app.use('/registro', registro_1.default);
// Levantar Servidor
server.start(function () {
    console.log("Servidor corriendo, puerto " + server.port);
});
